# import "heapq" to implement a heap queue
from heapq import heapify, heappush, heappop, heapreplace

# Create an empty min_heap list
min_heap = []

# Define a function that will print the heap
def print_min_heap():
	print("Min-heap elements:")
	for i in min_heap:
		print(i, end = ' ')
	print("\n")

# Push an element into an existing heap using the heappush method
heappush(min_heap, 10) # min_heap = [10]
heappush(min_heap, 30) # min_heap = [10, 30]
heappush(min_heap, 20) # min_heap = [10, 30, 20]
heappush(min_heap, 400) # min_heap = [10, 30, 20, 400]

# print_min_heap()

# prints the minimum value of the heap
print("The value of  heap is " + str(min_heap[0]))

element = heappop(min_heap)

print('The element popped from the min-heap is ' + str(element))
#print_min_heap()

# heapreplace method pops and returns the smallest element from the heap
heapreplace(min_heap, 35)
print_min_heap()

# Max heap and max heap operations

# Create an empty max_heap list
max_heap = []

# Use heapify to convert the list into a min-heap
heapify(min_heap)

heappush(max_heap, -1 * 10)		# max_heap = [-10]
heappush(max_heap, -1 * 30)		# max_heap = [-30, -10]
heappush(max_heap, -1 * 400)	# max_heap = [-400, -30, -10]
heappush(max_heap, -1 * 700)	# max_heap = [-700, -400, -30, -10]
heappush(max_heap, -1 * 20)		# max_heap = [-700, -400, -30, -10, -20]

# Get the element with the highest value
print('Head value of max-heap: ' + str(-1 * max_heap[0]))

def print_max_heap():
	print("Max-heap elements: ")
	for i in max_heap:
		print((-1 * i), end = " ")
	print("\n")

#print_max_heap()

element = heappop(max_heap)

print('The element popped from the max-heap is ' + str(-1 * element))

#print_max_heap()

# heapreplace in max heap
heapreplace(max_heap, -1 * 45)
print_max_heap()