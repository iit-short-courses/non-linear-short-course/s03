# import "heapq" to implement a heap queue
from heapq import heapify, heappush, heappop, heapreplace, nlargest, nsmallest

min_heap = []

heapify(min_heap)

heappush(min_heap, 13) 
heappush(min_heap, 20) 
heappush(min_heap, 30) 
heappush(min_heap, 40) 
heappush(min_heap, 41) 
heappush(min_heap, 51) 
heappush(min_heap, 100) 


print("min heap:  ", min_heap)

print("largest values:  ", nlargest(3, min_heap))

print("deleted item: ", heapreplace(min_heap, 24))
print("new min_heap:  ", min_heap)

max_heap = []

heapify(max_heap)

heappush(max_heap, -4) 
heappush(max_heap, -9) 
heappush(max_heap, -16) 
heappush(max_heap, -18) 
heappush(max_heap, -26) 
heappush(max_heap, -13) 
heappush(max_heap, -14) 

print("max heap:  ", max_heap)
print("2 largest:  ", nlargest(2, max_heap))
print("3 smallest values:  ", nsmallest(3, max_heap))

def heapsort(my_heap):    
    # Create an empty result list
    sorted_heap = []
    
    # Pop items from the heap and append them to the result list
    while my_heap:
        sorted_heap.append(heappop(my_heap))
    
    return sorted_heap

max_heap = heapsort(max_heap)
print("sorted heap:  ", max_heap)

